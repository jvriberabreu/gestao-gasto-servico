package br.com.santander;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@EnableRedisRepositories(basePackages = {"br.com.santander.*"})
@EntityScan(basePackages = {"br.com.santander.entity"})
@ComponentScan({"br.com.santander.repository", "br.com.santander.mapper", "br.com.santander"})
@EnableAutoConfiguration
@SpringBootApplication
public class GestaoGastoServicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestaoGastoServicoApplication.class, args);
	}

}
