package br.com.santander.service;

import java.util.List;

import org.springframework.stereotype.Component;

import br.com.santander.dto.CategoriaResponseDTO;

@Component
public interface CategoriaService {

	public CategoriaResponseDTO incluirCategoria();
	
	public List<CategoriaResponseDTO> listarCategorias();
	
}
