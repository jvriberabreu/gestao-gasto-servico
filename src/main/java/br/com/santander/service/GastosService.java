package br.com.santander.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.santander.dto.GastosResponseDTO;

@Component
public interface GastosService {

	public List<GastosResponseDTO> listarGastos();
	
	public GastosResponseDTO incluirGastos();
	
	public List<GastosResponseDTO> listarGastosPorData(Date data);
	
	public List<GastosResponseDTO> listarGastosPorCategoria(String categoria);
	
}
